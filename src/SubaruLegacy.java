
public class SubaruLegacy extends ACar implements ICar{
	
	public SubaruLegacy( int inYear, int inMileage )
	{
		super("Subaru", "Legacy", inYear);
		setMileage(inMileage);
	}

}
