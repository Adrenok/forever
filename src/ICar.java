/**
 * Different types in Java:
 * class
 * enum
 * primatives
 * interfaces
 * @author unouser
 *
 * A class is a combination of data and methods.
 * 
 * An interface only defines methods that it's subclasses will have.
 * By definition, everything in an interface is public.
 */
public interface ICar 
{
	String getMake();
	String getModel();
	int getYear();
	int getMileage();
}
